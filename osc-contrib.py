#
#   A contrib plugin for osc
#
#   This tool makes maintenance of openSUSE:Factory:Contrib more easier than an
#   universal osc commands.
#
#   Copyright: (c) 2009 Michal Vyskocil <mvyskocil@suse.cz>
#
#   Version: 0.1
#

@cmdln.option('-m', '--message', metavar='MESSAGE',
              help='the request message (optional)')
@cmdln.option('-i', '--id', metavar='REQUEST_ID',
              help='The concrete request id. Usefull whe multiple requests exists.')
@cmdln.option('-l', '--last-request', action='store_true', default=False,
              help='Use a last request (default False)')
@cmdln.option('-s', '--state', metavar='STATE',
              help='Show a requests with a specified state.')
@cmdln.option('-f', '--full-view', action='store_true', default=False,
              help='Make a full view for show command (default False)')
@cmdln.alias("cb")
def do_contrib(self, subcmd, opts, *args):
    """${cmd_name}: Handling a requests for Contrib

osc subcommand for maintenance of openSUSE Contrib repository. This command
tries to make the maintenance process easier than common osc commands. These
commands are derived from existing ones, but have a different arguments.

osc contrib show [PACKAGE]
Show all new requests towards Contrib. The optional argument package will
filter only a requests to it.

Options:
    -s, --state     filter the state (type 'any' for all)
    -f, --full-view a full view of requests

osc contrib new [DEST_PACKAGE]
osc contrib new PROJECT PACKAGE [DEST_PACKAGE]
A request for adding a new package to Contrib. When requesting from package dir
all necessary informations are readed from osc metadata. Only a DEST_PACKAGE
should be givven, if you want to have another name of your package in Contrib.

If you are in common dir, then you have to specify the PROJECT and PACKAGE
manually and DEST_PACKAGE is also optional.

osc contrib checkout PACKAGE
Checkout the requested package to the current dir.

Options:
    -i, --id        id of request (if multiple exists)
    -l, --last      use a last request (if multiple exists)


osc contrib [accept|decline|revoke] PACKAGE
Change the state of package to <state>

Options:
    -i, --id        id of request (if multiple exists)
    -l, --last      use a last request (if multiple exists)
    -m, --message   the submit message (optional for accept)
    """

    import types
    cmds = [cmd[12:] for cmd in dir(self) if cmd[0:12] == '_do_contrib_' and type(getattr(self, cmd)) == types.MethodType]
    if not args or args[0] not in cmds:
        raise oscerr.WrongArgs("Unknown contrib action. Choose one of %s." \
                                % ', '.join(cmds))
    
    command = args[0]

    self.project = 'openSUSE:Factory:Contrib'
    #self.project = 'home:mvyskocil'
    self.apiurl  = conf.config['apiurl']

    # call
    getattr(self, "_do_contrib_%s" % (command))(opts, args[1:])

def _sr_from_package(self, package, reqid=None, use_last=False, req_state=''):
    requests = get_submit_request_list(self.apiurl, self.project, package, req_state=req_state)
    if len(requests) == 0:
        raise oscerr.WrongArgs("No request for package %s found" % (package))
    elif len(requests) > 1:
        if use_last:
            requests = requests[-1:]
        elif reqid == None:
            raise oscerr.WrongArgs(
            "There are multiple requests (%s) towards package %s. Specify one by -i/--id, or use -l/--last-request argument!" %
            (", ".join([str(r.reqid) for r in requests]), package))
        else:
            ret = [req for req in requests if req.reqid == int(reqid)]
            if len(ret) == 0:
                raise oscerr.WrongArgs("The package %s and request id %s doesn't match! \
                        Use one of these (%s)" % (package, reqid, ", ".join([str(r.reqid) for r in requests])))
            requests = ret

    return requests[0]

def _do_contrib_show(self, opts, args):

    package = ''
    if len(args) > 0:
        package = args[0]

    state = opts.state or 'new'
    if state == 'any':
        state = ''
    
    srs = get_submit_request_list(self.apiurl, self.project, package, req_state=state)
    if opts.full_view:
        for sr in srs:
            print(sr)
    else:
        for sr in srs:
            print(sr.list_view())

def _do_contrib_new(self, opts, args):
    if is_package_dir(os.getcwdu()):
        src_project = store_read_project(os.getcwdu())
        src_package = store_read_package(os.getcwdu())
        dest_package = src_package
        if len(args) > 0:
            dest_package = args[0]
    else:
        if len(args) < 2:
            raise oscerr.WrongArgs("The source project and package names are mandatory!!")
        src_project, src_package = args[0], args[1]
        if not src_package in meta_get_packagelist(self.apiurl, src_project):
            raise oscerr.WrongArgs("Package '%s' don't exists in project '%s'" % (src_package, src_project))
        dest_package = src_package
        if len(args) == 3:
            dest_package = args[2]

    message = opts.message or "please add a '%s' to Contrib" % (dest_package)
    id = create_submit_request(self.apiurl, src_project, src_package, self.project, dest_package, message)
    print("Request id %s created" % (id))

def _do_contrib_accept(self, opts, args):
    
    return self._contrib_sr_change(opts, args, "accepted", 
           "Reviewed and checked OK.")

def _do_contrib_decline(self, opts, args):

    if not opts.message:
        raise oscerr.WrongArgs('A message is mandatory for decline')
    
    return self._contrib_sr_change(opts, args, "declined",
           opts.message)

def _do_contrib_revoke(self, opts, args):
    
    if not opts.message:
        raise oscerr.WrongArgs('A message is mandatory for decline')
    
    return self._contrib_sr_change(opts, args, "revoked",
           opts.message)


def _do_contrib_co(self, opts, args):
    return self._do_contrib_checkout(opts, args)

def _do_contrib_checkout(self, opts, args):
    package = args[0]
    if not package:
        raise oscerr.WrongArgs("The package names are mandatory!!")

    request = self._sr_from_package(package, opts.id, opts.last_request)

    checkout_package(self.apiurl,
            request.src_project, package,
            expand_link=True)

# the original API is *very* ugly!!
# return the meta in an xml form first
def _get_meta_xml(self, package):
    path = quote_plus(self.project),
    kind = 'prj'
    if package:
        path = path + (quote_plus(package),)
        kind = 'pkg'
    data = meta_exists(metatype=kind,
                       path_args=path,
                       template_args=None,
                       create_new=False)
    if data:
        return ET.fromstring(''.join(data))
    raise oscerr.PackageError('Meta data for package %s missing' % (package))

# return all persons from meta
def _get_persons_from_meta(self, meta):
    return meta.getiterator('person')

def _get_roles_from_meta(self, meta, role):
    assert(role in ['maintainer', 'bugowner'])
    return [p for p in self._get_persons_from_meta(meta) if p.get('role') == role]

def _has_user_role(self, meta, role, user):
    assert(role in ['maintainer', 'bugowner'])
    if not get_user_meta(self.apiurl, user):
        raise oscerr.WrongArgs("The user %s doesn't exists" % (user))

    return user in [p.get('userid') for p in self._get_roles_from_meta(meta, role)]

# from osc.core, FIXME, this is broken
# look at the svn, or send a patch to obs
def _addBugowner(self, apiurl, prj, pac, user):
    """ add a new bugowner to a package or project """
    path = quote_plus(prj),
    kind = 'prj'
    if pac:
        path = path + (quote_plus(pac),)
        kind = 'pkg'
    data = meta_exists(metatype=kind,
                       path_args=path,
                       template_args=None,
                       create_new=False)
                       
    if data and get_user_meta(apiurl, user) != None:
        tree = ET.fromstring(''.join(data))
        found = False
        for person in tree.getiterator('person'):
            if person.get('userid') == user and person.get('role') == 'bugowner':
                found = True
                print "user already exists"
                break
        if not found:
            # the xml has a fixed structure
            tree.insert(2, ET.Element('person', role='bugowner', userid=user))
            print 'user \'%s\' added to \'%s\'' % (user, pac or prj)
            edit_meta(metatype=kind,
                      path_args=path,
                      data=ET.tostring(tree))
    else:
        print "osc: an error occured"

def _contrib_sr_change(self, opts, args, action, message):

    if len(args) == 0:
        raise oscerr.WrongArgs('The package name is mandatory for %s' % (action))

    package = args[0]
    request = self._sr_from_package(package, opts.id, opts.last_request)
    
    id = str(request.reqid)

    # check the current state
    sr = get_submit_request(self.apiurl, id)

    if sr.state.name != 'new':
        print "The state of %s request was changed to '%s' by '%s'" % (package, sr.state.name, sr.state.who)
        res = raw_input("Do you want to change it to '%s'? [y/N] " % (action))
        if res != 'y' and res != 'Y':
            return

    # check before change of commit request
    is_new_package = not package in meta_get_packagelist(self.apiurl, self.project)
    if action == 'accepted' and is_new_package:
        essage = "You are now a maintainer of %s in openSUSE:Factory:Contrib" % (package)

    # change to the state action
    response = change_submit_request_state(self.apiurl, id, action, message)
    
    # change the state for a new packages
    if action == 'accepted' and is_new_package:
        # fix the maintainer and a bugowner
        meta = self._get_meta_xml(package)
        who = sr.statehistory[0].who
        if not self._has_user_role(meta, 'maintainer', who):
            delMaintainer(self.apiurl, self.project, package, sr.state.who)
            addMaintainer(self.apiurl, self.project, package, who)
        if not self._has_user_role(meta, 'bugowner', who):
            self._addBugowner(self.apiurl, self.project, package, who)

    print(response)
